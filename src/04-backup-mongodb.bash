#!/usr/local/bin/bash

BACKUP_DIR="/backup"

kubectl port-forward svc/mongo  27017:27017 &
child_pid=$!
sleep 5

# eseguo backup
mongodump --oplog --gzip --archive=mongodump-${BACKUP_DIR}/`date +%Y%m%d`.gz

kill $child_pid

# cancello i backup più vecchi di 7gg
find ${BACKUP_DIR} -name mongodump-*.gz -mtime -7 -exec rm -rf {} \;
