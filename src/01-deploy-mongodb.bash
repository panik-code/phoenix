#!/usr/local/bin/bash

# carico variabili
source config.bash

if [ -z $DB_USER -o -z $DB_PASS ]
then
    echo "Set DB_USER and DB_PASS variables"
    exit 1
fi


# preparo yaml for MongoDB
cat >mongodb.yaml<<EOF!
---
kind: StorageClass
apiVersion: storage.k8s.io/v1beta1
metadata:
  name: fast
provisioner: kubernetes.io/gce-pd
parameters:
  type: pd-ssd
---
apiVersion: v1
kind: Service
metadata:
  name: mongo
  labels:
    name: mongo
spec:
  ports:
  - port: 27017
    targetPort: 27017
  clusterIP: None
  selector:
    role: mongo
---
apiVersion: apps/v1
kind: StatefulSet
metadata:
  name: mongo
spec:
  serviceName: "mongo"
  replicas: 3
  selector:
    matchLabels:
      role: mongo
  template:
    metadata:
      labels:
        role: mongo
        environment: poc
    spec:
      terminationGracePeriodSeconds: 10
      containers:
        - name: mongo
          image: mongo
          command:
            - mongod
            - "--replSet"
            - rs0
            - "--bind_ip_all"
          ports:
            - containerPort: 27017
          volumeMounts:
            - name: mongo-persistent-storage
              mountPath: /data/db
        - name: mongo-sidecar
          image: cvallance/mongo-k8s-sidecar
          env:
            - name: MONGO_SIDECAR_POD_LABELS
              value: "role=mongo,environment=poc"
  volumeClaimTemplates:
  - metadata:
      name: mongo-persistent-storage
      annotations:
        volume.beta.kubernetes.io/storage-class: "fast"
    spec:
      accessModes: [ "ReadWriteOnce" ]
      resources:
        requests:
          storage: 10Gi
EOF!



# deploy mongodb
kubectl apply -f mongodb.yaml

# cicli di attesa per aspettare che il mongodb sia running
count=0
status="Starting"
while [ "$status" != "Running" ]
do
    sleep 5
    status=`kubectl get pods mongo-0 -o jsonpath="{.status.phase}"`
    echo "mongo-0 $status"
    let count++
    [ $count -gt 10 ] && exit 1
done

count=0
status="Starting"
while [ "$status" != "Running" ]
do
    sleep 5
    status=`kubectl get pods mongo-1 -o jsonpath="{.status.phase}"`
    echo "mongo-1 $status"
    let count++
    [ $count -gt 10 ] && exit 1
done

count=0
status="Starting"
while [ "$status" != "Running" ]
do
    sleep 5
    status=`kubectl get pods mongo-2 -o jsonpath="{.status.phase}"`
    echo "mongo-2 $status"
    let count++
    [ $count -gt 10 ] && exit 1
done

kubectl port-forward  svc/mongo 27017:27017 &
child_pid=$!
sleep 10

# init replicaset
mongo <<EOF!
rs.initiate( {  _id : "rs0",
   members: [
      { _id: 0, host: "mongo-0.mongo:27017" },
      { _id: 1, host: "mongo-1.mongo:27017" },
      { _id: 2, host: "mongo-2.mongo:27017" }
   ]
})
EOF!

sleep 5

# init db e user
mongo <<EOF!
use ${DATABASE}
db.createUser(
  {
    user: "${DB_USER}",
    pwd:  "${DB_PASS}",   // or cleartext password
    roles: [ { role: "readWrite", db: "test" },
             { role: "read", db: "reporting" } ]
  }
)
EOF!

kill $child_pid

