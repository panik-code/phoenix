#!/usr/local/bin/bash

# env
source config.bash


[ -d ${DOCKER_IMAGE} ] && rm -rf ${DOCKER_IMAGE}

gcloud -q container images delete ${GCP_REGISTRY}/${GCP_PROJECT}/${DOCKER_IMAGE}

rm *.yaml

kubectl delete deployment phoenix-deployment 
kubectl delete statefulset mongo
kubectl delete service mongo
kubectl delete pvc -l role=mongo
kubectl delete storageclass fast

gcloud -q container clusters delete ${GCP_K8S}
gcloud -q compute networks subnets delete ${GCP_SUBNET}
gcloud -q compute networks delete ${GCP_NETWORK}

