#!/usr/local/bin/bash

# env variables
source config.bash

# gcp region and project
gcloud config set project ${GCP_PROJECT}
gcloud config set compute/region ${GCP_REGION}
gcloud config set compute/zone ${GCP_ZONE}

# create net & subnet
gcloud compute networks create ${GCP_NETWORK} --subnet-mode custom
gcloud compute networks subnets create ${GCP_SUBNET}  --network ${GCP_NETWORK}  --range ${GCP_SUBNET_CIDR}

# create GKE & get credentials
gcloud container clusters create ${GCP_K8S} --network=${GCP_NETWORK} --subnetwork=${GCP_SUBNET} --enable-stackdriver-kubernetes
gcloud container clusters get-credentials ${GCP_K8S}

# verify k8s nodes are up abd running
kubectl get nodes -o wide
kubectl get --raw='/readyz?verbose'
