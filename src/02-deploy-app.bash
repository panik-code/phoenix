#!/usr/local/bin/bash

# env
source config.bash

if [ -z $DB_USER -o -z $DB_PASS ]
then
    echo "Set DB_USER and DB_PASS variables"
    exit 1
fi
DB_CONNECTION_STRING="mongodb://${DB_USER}:${DB_PASS}@mongo:27017/${DATABASE}"

# clone del repo
git clone ${APP_REPO} ${DOCKER_IMAGE}

# build docker image

cd ${DOCKER_IMAGE}

cat >Dockerfile  <<EOF!
FROM node:8.11.1
WORKDIR /usr/src/app
COPY package*.json ./
RUN npm install
COPY . .
CMD [ "npm", "start" ]
EOF!

cat >.dockerignore <<EOF!
node_modules
npm-debug.log
EOF!

# image build
docker build --no-cache -t ${DOCKER_IMAGE} .

# pubblica su GCP docker registry
docker tag ${DOCKER_IMAGE} ${GCP_REGISTRY}/${GCP_PROJECT}/${DOCKER_IMAGE}
docker push ${GCP_REGISTRY}/${GCP_PROJECT}/${DOCKER_IMAGE}

# verifica
gcloud container images list

cd ..

cat >phoenix.yaml<<EOF!
---
apiVersion: v1
kind: Service
metadata:
  name: phoenix-panik
  labels:
    name: phoenix
spec:
  type: LoadBalancer
  ports:
  - port: ${SVC_PORT}
    targetPort: ${SVC_PORT}
  selector:
    app: phoenix


---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: phoenix-deployment
spec:
  selector:
    matchLabels:
      app: phoenix
  replicas: 2
  template:
    metadata:
      labels:
        app: phoenix
    spec:
      containers:
      - name: phoenix
        image: gcr.io/paniktest/panik-phoenix
        resources:
          limits:
            memory: 512Mi
            cpu: "1"
          requests:
            memory: 256Mi
            cpu: "0.4"
        env:
          - name: PORT
            value: "${SVC_PORT}"
          - name: DB_CONNECTION_STRING
            value: "${DB_CONNECTION_STRING}"
        livenessProbe:
          httpGet:
            path: /
            port: ${SVC_PORT}
          initialDelaySeconds: 30
          periodSeconds: 15
EOF!

# deploy app
kubectl apply -f phoenix.yaml

