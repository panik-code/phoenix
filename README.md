# Risposta per l'esercizio cloud-phoenix-kata

Ho pensato la risposta presupponendo di deployare il frontend ed il database su un cluster kubernetes, ben sapendo che la soluzione di portare un database su questo tipo di infrastruttura possa rappresentare un limite alle performance. La soluzione ottimale sarebbe quella di deployare il database MongoDB su un server dedicati, con buone performance disco.

Ho pensato di utilizzare un cluster kubernetes gestito da un cloud provider ed in particolare Google Cloud Platform (**GCP**). Gli script che automatizzano la predisposizione dell'infrastruttura su GCP hanno come assunto che siano lanciati con credenziali già disponibili e servizi API di GCP già attivati. Non ho quindi inserito negli script la fase di attivazione dei servizi GCP, come la creazione del project, del service account e delle abilitazioni alle API che si ottengono con comandi simili a questi

```
gcloud services enable containerregistry.googleapis.com
gcloud auth configure-docker
gcloud services enable container.googleapis.com
```

Ho supposto di usare questi servizi GCP

- GKE per il cluster Kubernetes gestito
- Google Container Registry
- Google Cloud Build
- Google Monitor

Ho utilizzato script bash, da eseguire in successione, per la predisposizione dell'infrastruttura, del deploy del database e dell'applicazione. Di seguito l'elenco degli script ed descrizione.

- [00-create-infrastructure.bash](src/00-create-infrastructure.bash)
- [01-deploy-mongodb.bash](src/01-deploy-mongodb.bash)
- [02-deploy-app.bash](src/02-deploy-app.bash)
- [04-backup-mongodb.bash](src/04-backup-mongodb.bash)
- [99-clean.bash](src/99-clean.bash)

Gli script devon essere lanciati avenro impostato le variabili di ambiente:

- DB_USER : utenza del database MongoDB
- DB_PASS : password del database MongoDB

```
env | grep DB
DB_PASS=phoenix-pass
DB_USER=phoenix
```

Altre variabili di ambiente sono configurate tramite [config.bash](src/config.bash)

# Creazione infrastruttura

Tramite lo script [00-create-infrastructure.bash](src/00-create-infrastructure.bash) si esegue la creazione della network, della subnet e del cluster GKE e si eseguono due comanadi finali per verificare che il cluster sia attivo.

- [Scrennshot cluster GKE k8s](img/k8s.png)

# Deploy MongoDB

Lo script [01-deploy-mongodb.bash](src/01-deploy-mongodb.bash) effettua il deploy di un cluster a tre nodi, lo configura in replicaset ed imposta un database con password.

- [Screenshot storage MongoDB](img/mongo-storage.png)

# Deploy front-end

Lo script [02-deploy-app.bash](src/02-deploy-app.bash) effettua il clone del repository dell'app, costruisce l'immagine e la pubblica sul docker registry di GCP. Scrive i file yaml per deployare ed effettua il deploy. Il deployment k8s consente il riavvio in caso di crash; provato chiamando /CRASH. L'app è deployata ed è configurato un service di frontend con un load balancer GCP. 

- [Screenshot Google registry](img/registry.png)
- [Screenshot pod](img/pod.png)
- [Screenshot service](img/service.png)
- [Screenshot home page](img/home.png)

# Autoscaling

Non mi è mai capitato di scalare su questa metrica. Ho provato ad impostare Prometheus ma non ci sono riuscito. Lato GCP, con l'integrazione di stackdriver e le external metrics potrei agganciare le metriche del bilanciatore ma nessuna di quelle disponibile è adattabile alle request/s. Quindi per questa non ho adesso una soluzione.

# Backup MongoDB

Lo script [04-backup-mongodb.bash](src/04-backup-mongodb.bash) esegue i backup presupponendo di avere a disposizione un server Linux su cui attivarlo in crontab, una volta al giorno. Il server deve avere kubectl configurato per accedere al cluster.

# Alerting per uso CPU

Per l'alerting, avendo attivato il cluster GKE con l'integrazione stackdriver, imposto le soglie basandomi sulle metriche disponibili direttamente su GCP. Di seguito alcuni screenshot della configurazione. L'alert policy è impostata, come esempio, per avvertire via email.

- [Screenshot configurazione alert cpu dei nodi worker](img/alert01.png)
- [Screenshot configurazione alert notify via email](img/alert02.png)
- [Screenshot configurazione alert cpu container](img/alert03.png)
- [Screenshot configurazione elenco alert ](img/alert04.png)

# CI/CD per codice

Per l'implementazione di una catena CI/CD non ho molta esperienza. Quello che ho fatto è configurare un trigger a livello di integrazione fra il repository BitBucket, dove ho clonato l'app, e GCP Cloud Build. Ad ogni modifica del codice viene eseguita una build dell'immagine e viene caricata sul registry GCP. Si può estendere inserendo controlli su codice e deploy su GKE.

- [Screenshot trigger bitbucket](img/bitbucket01.png)
- [Screenshot build image](img/bitbucket02.png)
- [Screenshot build image](img/bitbucket03.png)

# Clean

Lo script [99-clean.bash](src/99-clean.bash) cancella tutti i deloyment e l'infrastruttura creata.







